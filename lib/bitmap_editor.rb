class BitmapEditorFileReader
  def run(file)
    return puts "please provide correct file" if file.nil? || !File.exists?(file)

    bitmap_editor = BitmapEditor.new
    command_reader = CommandReader.new(bitmap_editor)
    File.open(file).each do |line|
      command_reader.defer_to_method(line)
    end
  end
end

class BitmapEditor
  def create_table(width, height)
    _validate_height_and_width(width, height)
    @width, @height = width, height
    @table = Array.new(height) {Array.new(width, "O")}
  end

  def clear
    _check_table_exists()
    @table.each do |row|
      row.each do |element|
        element = "O"
      end
    end
  end

  def colour_pixel(x, y, colour)
    _check_table_exists()
    x, y = _change_strings_to_integers([x, y])
    _validate_rows_and_columns([x], [y])
    @table[y-1][x-1] = colour
  end

  def vertical_line(x, y_1, y_2, colour)
    _check_table_exists()
    _validate_rows_and_columns([x], [y_1, y_2])
    min_y, max_y = [y_1, y_2].minmax
    y = min_y
    while y <= max_y
      colour_pixel(x, y, colour)
      y = y + 1
    end
  end

  def horizontal_line(x_1, x_2, y, colour)
    _check_table_exists()
    _validate_rows_and_columns([x_1, x_2], [y])
    min_x, max_x = [x_1, x_2].minmax
    x = min_x
    while x <= max_x
      colour_pixel(x, y, colour)
      x = x + 1
    end
  end

  def display
    _check_table_exists()
    @table.each do |row|
      row.each do |pixel|
        print pixel
      end
      puts ""
    end
  end

  def _check_table_exists
    if @table == nil
      raise "Table has not yet been created. Please create table before other commands"
    end
  end


  def _validate_height_and_width(width, height)
    if width <= 0 or height <= 0
      raise "Height and Width must both be greater than 0"
    end
  end

  def _validate_rows_and_columns(columns, rows)
    rows.each do |row|
      if row < 1 or row > @height
        raise "Row must be between 1 and the height (inclusive)"
      end
    end

    columns.each do |column|
      if column < 1 or column > @width
        raise "Column must be between 1 and the width (inclusive)"
      end
    end
  end

  def _change_strings_to_integers(args)
    integer_args = []
    begin
      args.each do |arg|
        int_arg = arg.to_i
        integer_args.push(int_arg)
      end
    rescue SyntaxError
      raise "All numbers must be integers"
    end

    return integer_args
  end
end


class CommandReader
  def initialize(bitmap_editor)
    @bitmap_editor = bitmap_editor
  end

  @@command_map = {
    "I" => "create_table",
    "C" => "clear",
    "L" => "colour_pixel",
    "V" => "vertical_line",
    "H" => "horizontal_line",
    "S" => "display"
  }

  @@argument_types_map = {
    "I" => ["Integer", "Integer", "String"],
    "C" => [],
    "L" => ["Integer", "Integer", "String"],
    "V" => ["Integer", "Integer", "Integer", "String"],
    "H" => ["Integer", "Integer", "Integer", "String"],
    "S" => []
  }

  def defer_to_method(command_string)
    method_code, *args = command_string.split

    method_string = @@command_map[method_code]

    argument_types = @@argument_types_map[method_code]
    args = _convert_args_types(args, argument_types)

    @bitmap_editor.send(method_string, *args)
  end

  def _convert_args_types(args, argument_types)
    arg_type = nil
    index = nil
    begin
      args.each_with_index do |arg, index|
        arg_type = argument_types[index]
        args[index] = send(arg_type, arg)
      end
    rescue ArgumentError
      raise "Argument number #{index} must be of type #{arg_type}"
    end
    return args
  end
end
