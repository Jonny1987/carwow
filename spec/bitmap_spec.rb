require "bitmap_editor"
require "rspec"

def set_table(bitmap_editor, x, y)
  bitmap_editor.instance_variable_set(:@table, Array.new(y) {Array.new(x, "O")})
  bitmap_editor.instance_variable_set(:@width, x)
  bitmap_editor.instance_variable_set(:@height, y)
end
 
describe BitmapEditor do

  describe '#CreateTable' do
    before(:each) do
      @bitmap_editor = BitmapEditor.new
    end

    it 'should raise exception on negative dimension argument' do
      expect{@bitmap_editor.create_table(4, -1)}.to raise_error(RuntimeError)
    end

    it 'should raise exception on zero dimension argument' do
      expect{@bitmap_editor.create_table(4, 0)}.to raise_error(RuntimeError)
    end

    it 'should create table of given positive dimensions' do
      @bitmap_editor.create_table(4, 5)
      expect(@bitmap_editor.instance_variable_get(:@table).length).to eq(5)
      expect(@bitmap_editor.instance_variable_get(:@table)[0].length).to eq(4)
    end
  end
 
  describe '#Clear' do
    before(:each) do
      @bitmap_editor = BitmapEditor.new
    end

    it 'should raise exception if table not created yet' do
      expect{@bitmap_editor.clear()}.to raise_error(RuntimeError)
    end

    it 'should clear table if table created' do
      set_table(@bitmap_editor, 4, 5)
      @bitmap_editor.clear()

      @bitmap_editor.instance_variable_get(:@table).each do |row|
        row.each do |element|
          expect(element).to eq("O")
        end
      end
    end
  end

  describe '#ColourPixel' do

    before(:each) do
      @bitmap_editor = BitmapEditor.new
    end

    it 'should raise exception if table not created yet' do 
      expect{@bitmap_editor.colour_pixel(4, 5, "C")}.to raise_error(RuntimeError)
    end

    it 'should raise exception if negative index given' do
      set_table(@bitmap_editor, 4, 5)

      expect{@bitmap_editor.colour_pixel(2, -1, "C")}.to raise_error(RuntimeError)
    end

    it 'should raise exception if index is larger than table dimension' do
      set_table(@bitmap_editor, 4, 5)

      expect{@bitmap_editor.colour_pixel(2, 6, "C")}.to raise_error(RuntimeError)
    end

    it 'should change colour of element at given location' do
      set_table(@bitmap_editor, 4, 5)

      @bitmap_editor.colour_pixel(2, 2, "C")

      @bitmap_editor.instance_variable_get(:@table).each_with_index do |row, y|
        row.each_with_index do |element, x|
          if x == 1 and y == 1
            expect(element).to eq("C")
          else
            expect(element).to eq("O")
          end
        end
      end
    end
  end

  describe '#VerticalLine' do
    before(:each) do
      @bitmap_editor = BitmapEditor.new
    end

    it 'should raise exception if table not created yet' do 
      expect{@bitmap_editor.vertical_line(2, 1, 3, "C")}.to raise_exception(RuntimeError)
    end

    it 'should raise exception if negative vertical index given' do 
      set_table(@bitmap_editor, 4, 5)
      expect{@bitmap_editor.vertical_line(2, -1, 3, "C")}.to raise_exception(RuntimeError)
    end

    it 'should raise exception if negative horizontal index given' do 
      set_table(@bitmap_editor, 4, 5)
      expect{@bitmap_editor.vertical_line(-2, 1, 3, "C")}.to raise_exception(RuntimeError)
    end

    it 'should raise exception if vertical index larger than table dimension' do 
      set_table(@bitmap_editor, 4, 5)
      expect{@bitmap_editor.vertical_line(2, 1, 6, "C")}.to raise_exception(RuntimeError)
    end

    it 'should raise exception if horizontal index larger than table dimension' do 
      set_table(@bitmap_editor, 4, 5)
      expect{@bitmap_editor.vertical_line(6, 1, 3, "C")}.to raise_exception(RuntimeError)
    end

    it 'should draw vertical line at given location' do
      set_table(@bitmap_editor, 4, 5)

      @bitmap_editor.vertical_line(2, 1, 3, "C")

      @bitmap_editor.instance_variable_get(:@table).each_with_index do |row, y|
        row.each_with_index do |element, x|
          if x == 1 and y >= 0 and y <= 2
            expect(element).to eq("C")
          else
            expect(element).to eq("O")
          end
        end
      end
    end
  end

  describe '#HorizontalLine' do
    before(:each) do
      @bitmap_editor = BitmapEditor.new
    end

    it 'should raise exception if table not created yet' do 
      expect{@bitmap_editor.horizontal_line(1, 3, 2, "C")}.to raise_exception(RuntimeError)
    end

    it 'should raise exception if negative vertical index given' do 
      set_table(@bitmap_editor, 4, 5)
      expect{@bitmap_editor.horizontal_line(1, 3, -2, "C")}.to raise_exception(RuntimeError)
    end

    it 'should raise exception if negative horizontal index given' do 
      set_table(@bitmap_editor, 4, 5)
      expect{@bitmap_editor.horizontal_line(-1, 3, 2, "C")}.to raise_exception(RuntimeError)
    end

    it 'should raise exception if vertical index larger than table dimension' do 
      set_table(@bitmap_editor, 4, 5)
      expect{@bitmap_editor.horizontal_line(1, 3, 6, "C")}.to raise_exception(RuntimeError)
    end

    it 'should raise exception if horizontal index larger than table dimension' do 
      set_table(@bitmap_editor, 4, 5)
      expect{@bitmap_editor.horizontal_line(1, 6, 2, "C")}.to raise_exception(RuntimeError)
    end

    it 'should draw horizontalline at given location' do
      set_table(@bitmap_editor, 4, 5)

      @bitmap_editor.horizontal_line(1, 3, 2, "C")

      @bitmap_editor.instance_variable_get(:@table).each_with_index do |row, y|
        row.each_with_index do |element, x|
          if y == 1 and x >= 0 and x <= 2
            expect(element).to eq("C")
          else
            expect(element).to eq("O")
          end
        end
      end
    end
  end

  describe '#Display' do
    before(:each) do
      @bitmap_editor = BitmapEditor.new
    end

    it 'should raise exception if table not created yet' do
      expect{@bitmap_editor.display()}.to raise_exception(RuntimeError)
    end

    it 'should display table if table created' do
      set_table(@bitmap_editor, 4, 5)
      $stdout = StringIO.new

      @bitmap_editor.display()

      output = "OOOO\nOOOO\nOOOO\nOOOO\nOOOO\n"
      expect(output).to eq($stdout.string)
    end
  end
end

describe CommandReader do

  class MockBitmapEditor
    def initialize
      @call_record = {
        "create_table" => nil,
        "clear" => nil,
        "colour_pixel" => nil,
        "vertical_line" => nil,
        "horizontal_line" => nil,
        "display" => nil
      }
    end

    def create_table(*args)
      @call_record["create_table"] = args
    end

    def clear
      @call_record["clear"] = []
    end

    def colour_pixel(*args)
      @call_record["colour_pixel"] = args
    end

    def vertical_line(*args)
      @call_record["vertical_line"] = args
    end

    def horizontal_line(*args)
      @call_record["horizontal_line"] = args
    end

    def display
      @call_record["display"] = []
    end

    def get_args_of_call(method_string)
      return @call_record[method_string]
    end
  end

  describe '#DeferToMethod' do
    before(:each) do
      @mock_editor = MockBitmapEditor.new
      @command_reader = CommandReader.new(@mock_editor)
    end

    it 'should call create_table method' do
      command = "I 1 2"

      @command_reader.defer_to_method(command)

      expect(@mock_editor.get_args_of_call("create_table")).to eq([1, 2])
    end

    it 'should call clear method' do
      command = "C"

      @command_reader.defer_to_method(command)

      expect(@mock_editor.get_args_of_call("clear")).to eq([])
    end

    it 'should call colour_pixel method' do
      command = "L 3 4 A"

      @command_reader.defer_to_method(command)

      expect(@mock_editor.get_args_of_call("colour_pixel")).to eq([3, 4, "A"])
    end

    it 'should call vertical_line method' do
      command = "V 5 6 7 B"

      @command_reader.defer_to_method(command)

      expect(@mock_editor.get_args_of_call("vertical_line")).to eq([5, 6, 7, "B"])
    end

    it 'should call horizontal_line method' do
      command = "H 8 9 10 C"

      @command_reader.defer_to_method(command)

      expect(@mock_editor.get_args_of_call("horizontal_line")).to eq([8, 9, 10, "C"])
    end

    it 'should call print method' do
      command = "S"

      @command_reader.defer_to_method(command)

      expect(@mock_editor.get_args_of_call("display")).to eq([])
    end
  end
end
